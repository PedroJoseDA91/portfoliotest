Cypress.config('baseUrl', 'https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/')

describe("Portfolio Api Test", () => {
    it("Creates, gets and Deletes", () => {
        cy.request('DELETE', '/portfolio/test').then(() => {
            const item = {
                "title": "Software Engineer",
                "id": "test",
                "email": "valid@email.local"
            }
            cy.request('POST', '/portfolio', item).then(() => {
                cy.request('GET', '/portfolio/test').then((response) => {
                    expect(response.body).to.have.property('email', 'valid@email.local')
                    cy.request('DELETE', '/portfolio/test')

                })
            })
        })
    })

    it("Checks for invalid emails", () => {
        const item = {
            "title": "Software Engineer",
            "id": "test_email",
            "email": "invalid_email.local"
        }
        cy.request({
            method: 'POST',
            url: '/portfolio',
            failOnStatusCode: false,
            body: item
        }).then((response) => {
            expect(response).to.have.property('status', 400)
            cy.request({
                method: 'GET',
                url: '/portfolio/test_email',
                failOnStatusCode: false,

            }).then((response) => {
                expect(response).to.have.property('status', 404)
            })
        })
    })
    it('PUT - Update', () => {
        const item = {
            "title": "Software Engineer"
        }
        cy.request('PUT', '/portfolio/pdeaguas', item).then((response) => {
            expect(response.body).to.have.property('title', 'Software Engineer')
        })
    })

    it('GET - One', () => {
        cy.request('GET', '/portfolio/pdeaguas').then((response) => {
            expect(response).to.have.property('status', 200)
        })
    })


    it('GET - Many', () => {
        cy.request('GET', '/portfolio').then((response) => {
            expect(response).to.have.property('status', 200)
        })

    })
})